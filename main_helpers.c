#include "main_helpers.h"

#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <termios.h>

//Zoli
char* read_name(int length) {
	int i = 0;
	char* name = malloc(length+1);
	char current_char = 0;
	while (current_char != '\n') {
		current_char = fgetc(stdin);
		if (i < length && current_char != '\n') {
			name[i] = current_char;
			i++;
		}
	}
	name[i] = 0;
	return name;
} // read_name
//Levi
void clear_screen() {
	system("clear");
} // clear_screen

// kikapcsolja a bemenet puffereleset es visszairasat
// plusz ejrejti a kurzort is
// nyilvan internet lett hasznalva ehhez a reszhez
//Levi
void term_raw_mode() {
	system("stty raw -echo");
	printf("\e[?25l");
} // term_raw_mode
//Levi
void term_reset() {
	system("stty cooked echo");
	printf("\e[?25h");
} // term_reset
