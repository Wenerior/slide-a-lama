#ifndef _LIBLAMA_H_
#define _LIBLAMA_H_

#define TABLE_SIZE 5

#define index_by_coord(x, y) (((x) * TABLE_SIZE) + (y))
#define print_spaces(n) for (int _i = 0; _i < (n); _i++)putchar(' ')

void print_state(int* table, int* fruits, char** players, int* points, int current_player, int inserted_fruit_pos);//
void fill_table(int* table);
void tile_gravity(int* table);
void insert_fruit(int* table, int where, int fruit);
struct match find_match(int* table);
int match_to_points(int* table, struct match m);
int move_cursor(int direction, int prev_state);//
void fill_fruits(int* fruits);
int use_fruit(int* fruits);
void print_tile(int fruit);//
int find_winner(int* points);

struct match {
	int length;
	int direction; // 0 = vizszintes, 1 = fuggoleges
	int x;
	int y;
	int fruit;
};

#define SEVEN_STRING "\e[45m 7 \e[0m"
#define BAR_STRING "\e[46m B \e[0m"
#define CHERRY_STRING "\e[41m \% \e[0m"
#define PLUM_STRING "\e[44m Q \e[0m"
#define PEAR_STRING "\e[42m @ \e[0m"
#define BANANA_STRING "\e[43m C \e[0m"
#define BELL_STRING "\e[47m A \e[0m"
#define EMPTY_STRING "   "

#define EMPTY 7

#endif
