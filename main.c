#include "liblama.h"
#include "main_helpers.h"

#include <stdio.h>
#include <stdlib.h>
//Levi 30% Zoli 70%
int main() {
	char** players = (char**)malloc(sizeof(char*) * 2);
	clear_screen();
	printf("Elso jatekos neve: ");
	players[0] = read_name(20);
	printf("\nMasodik jatekos neve: ");
	players[1] = read_name(20);
	int fruits[3];
	int table[25];
	int points[2];
	
	points[0] = 0;
	points[1] = 0;
	
	fill_table(table);
	fill_fruits(fruits);
	term_raw_mode();
	int current_player = 0;
	int inserted_fruit_pos = 0;
	
	for (int keypress = 0, exit_loop = 0;; keypress = fgetc(stdin)) {

		
		switch (keypress) {
			case 3: // ctrl + c kilepes
				exit_loop = 1;
			case 'w':
			case 'W':
				inserted_fruit_pos = move_cursor(0, inserted_fruit_pos);
				break;
			case 's':
			case 'S':
				inserted_fruit_pos = move_cursor(1, inserted_fruit_pos);
				break;
			case 'a':
			case 'A':
				inserted_fruit_pos = move_cursor(2, inserted_fruit_pos);
				break;
			case 'd':
			case 'D':
				inserted_fruit_pos = move_cursor(3, inserted_fruit_pos);
				break;
			case 27: // a nyilak 3 karaktert adnak vissza
				if (fgetc(stdin) == 91) {
					switch (fgetc(stdin)) {
						case 65:
							inserted_fruit_pos = move_cursor(0, inserted_fruit_pos);
							break;
						case 66:
							inserted_fruit_pos = move_cursor(1, inserted_fruit_pos);
							break;
						case 68:
							inserted_fruit_pos = move_cursor(2, inserted_fruit_pos);
							break;
						case 67:
							inserted_fruit_pos = move_cursor(3, inserted_fruit_pos);
							break;
							
					}
				}
				break;
			case 13: // enter
				insert_fruit(table, inserted_fruit_pos, use_fruit(fruits));
				for (struct match m = find_match(table); m.length != 0; m = find_match(table)) {
					points[current_player] += match_to_points(table, m);
				}
				current_player = !current_player;
				break;
				
		}
		//print_state(int* table, int* fruits, char** players, int* points, int current_player, int inserted_fruit_pos);
		if (find_winner(points) != -1) {
			clear_screen();
			printf("%s nyert.\n\rSzeretnetek uj kort jatszani? [I/*]\n\r", points[0] > points[1] ? players[0] : players[1]);
			char reply = fgetc(stdin);
			if (reply == 'I' || reply == 'i' || reply == 13) {
				points[0] = 0;
				points[1] = 0;
				fill_table(table);
				fill_fruits(fruits);
			} else {
				exit_loop = 1;
			}
		}
		if (exit_loop) break;
		clear_screen();
		print_state(table, fruits, players, points, current_player, inserted_fruit_pos);
	}
	
	free(players[0]);free(players[1]);free(players);
	term_reset();
	return 0;
}
