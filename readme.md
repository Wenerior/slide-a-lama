# Slide-a-lama

### Progalap tg projekt / Készítették: Ács Zoltán Attila és Kishalmi Levente

A jelenlegi implementáció csinál egy táblázatot, feltölti random adatokkal és kirajzolja.

TODO

- Szétszervezni libekbe a programot
- Buildsystem megírása (szerintem cmake kéne)
- Tesztek készítése
- Program funkcióinak megvalósítása
    - Tábla kirajzolása szépen (színek, emojik)
    - Billentyűzet navigáció
    - Következő gyümölcsök megjelenítése
    - Lámák számának megjelenítése
    - Meg úgy egyáltalán működjön a játék
