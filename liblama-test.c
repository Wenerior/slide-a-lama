#include <CUnit/CUnit.h>
#include "liblama.h"
#include "liblama-test.h"

extern int boolean=0;
//Levi
static void test_fill_table(){
        int table[25];
        fill_table(table);
        for(int i = 0; i < 25; i++) {
            if(table[i]>=0 && table[i]<=6){
                boolean=1;
            }else{
                boolean =0;
                break;
            }
        }
        CU_ASSERT_TRUE(boolean==1);
}
//Zoli
static void test_tile_gravity(){
    int table[25];
    for(int i = 0; i < 25; i++) {
            table[i] = 0;
    }
    table[20]=7;
    tile_gravity(table);
    boolean=0;
        if(table[0]==7){
            boolean=1;
        }
    CU_ASSERT_TRUE(boolean==1);
}
//Levi
static void test_insert_fruit(){
    int table[25];
    for(int i = 0; i < 25; i++) {
        table[i]=0;
    }
    insert_fruit(table,1,6);
    boolean=0;
    for(int i = 0; i < 25; i++) {
       if(table[i]==6){
           boolean=1;
       }
    }
    CU_ASSERT_TRUE(boolean==1);
}
//Levi
static void test_find_match() {
    int table[25];
    int sz=0;
    for(int i = 0; i < 25; i++) {
        if(sz==0){
            table[i]=0;
            sz=1;
    }else{
            table[i]=1;
            sz=0;
        }
    }
    for(int i = 0; i < 3; i++){
        table[i]=2;
    }
    struct match m = find_match(table);
    boolean=0;
    if(m.length==3&&m.direction==0&&m.fruit==2&&m.x==0&&m.y==0){
        boolean=1;
    }
    CU_ASSERT_TRUE(boolean==1);


}


//Levi
static void test_match_to_points(){
    int table[25];
    int sz=0;
    for(int i = 0; i < 25; i++) {
        if(sz==0){
            table[i]=0;
            sz=1;
        }else{
            table[i]=1;
            sz=0;
        }
    }
    for(int i = 0; i < 3; i++){
        table[i]=2;
    }
    struct match m = find_match(table);
   int a=match_to_points(table,m);
   boolean=0;
   if(a==75){
       for(int i = 0; i < 3; i++){
           if(table[i]==7){
               boolean=1;
           }else{
               boolean=0;
               break;
           }
       }
   }
    CU_ASSERT_TRUE(boolean==1);
}


//Levi
static void test_fill_fruits(){
    int fruits[3];
    fill_fruits(fruits);
    for(int i = 0; i < 3; i++){
        if(fruits[i]<=6&&fruits[i]>=0){
            boolean=1;
        }else{
            boolean=0;
            break;
        }
    }
    CU_ASSERT_TRUE(boolean==1);
}
//Levi
static void test_use_fruit(){
    int fruits[3];
    for(int i = 0; i < 3; i++){
        fruits[i]=i;
    }
    int a,boolean;
    a= use_fruit(fruits);
    if(a==0 && fruits[0]==1 && fruits[1]== 2 && fruits[2]<=6&&fruits[2]>=0){
        boolean=1;
    }else{
        boolean=0;
    }
    CU_ASSERT_TRUE(boolean==1);
}
//Zoli
static void test_find_winner(){
    int points[2];
    points[0]=0;
    points[1]=250;
    int a=find_winner(points);
    boolean=0;
    if(a==1){
        boolean=1;
    }
    CU_ASSERT_TRUE(boolean==1);
}
//Levi 80% Zoli 20%
CU_TestInfo liblama_tests[] = {
        {"A tabla feltoltese.", test_fill_table},
       {"Elemek leesese.",test_tile_gravity},
        {"Elem hozzaadasa.",test_insert_fruit},
        {"Eggyezes megtalalasa.",test_find_match},
        {"Eggyezes eltuntetese es kiertekelese.",match_to_points},
        {"Gyumolcsok feltoltese.",test_fill_fruits},
        {"Gyumolcs kivalasztasa.",test_use_fruit},
        {"Gyoztes megtalalasa.", test_find_winner},
        CU_TEST_INFO_NULL
};