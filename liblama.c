#include "liblama.h"

#include <stdio.h>
#include <stdlib.h>
#include <time.h> // a random szamokhoz

int values[7] = {150, 100, 70, 40, 30, 20, 10};

// tabla letrehozasa
//Levi
void fill_table(int* table) {
	// tabla lefoglalasa es feltoltese random adatokkal
	srand(time(NULL));
	for (int i = 0; i < TABLE_SIZE * TABLE_SIZE; i++) {
			table[i] = rand() % 7;
	}
    // ha talaunk olyan sort vagy oszlopot, amibol rogton pont jonne ki, abban kicsereljuk a masodik elemet valami masra
	//Zoli
    for (struct match m = find_match(table); m.length != 0; m = find_match(table)) {
		int possible_fruits[6];
		int possible_fruit_index = 0;
		for (int i = 0; i < 7; i++) {
			if (i != m.fruit) {
				possible_fruits[possible_fruit_index] = i;
				possible_fruit_index++;
			}
		}
		table[index_by_coord(m.x + m.direction, m.y + 1 - m.direction)] = possible_fruits[rand() % 6];
	}
} // fill_table
//Zoli
int move_cursor(int direction, int prev_state) {
	switch (direction) {
		case 0: // fel
			switch (prev_state) {
				case 0:
				case 1:
				case 2:
				case 3:
				case 4:
					return prev_state + 1;
				case 10:
				case 11:
				case 12:
				case 13:
				case 14:
					return prev_state - 1;
			}
			break;
		case 1: // le
			switch (prev_state) {
				case 0:
					return 14;
				case 1:
				case 2:
				case 3:
				case 4:
				case 5:
					return prev_state - 1;
				case 9:
				case 10:
				case 11:
				case 12:
				case 13:
					return prev_state + 1;
				case 14:
					return 0;
			}
			break;
		case 2: // balra
			switch (prev_state) {
				case 5:
				case 6:
				case 7:
				case 8:
				case 9:
				case 10:
					return prev_state - 1;
				case 14:
					return 0;
			}
			break;
		case 3: // jobbra
			switch (prev_state) {
				case 0:
					return 14;
				case 4:
				case 5:
				case 6:
				case 7:
				case 8:
				case 9:
					return prev_state + 1;
			}
			break;
	}
	return prev_state;
			
}

// tabla (es minden is) kiiratasa
//Levi 20% Zoli 80%
void print_state(int* table, int* fruits, char** players, int* points, int current_player, int inserted_fruit_pos) {
	// elso sor
	if (inserted_fruit_pos >= 5 && inserted_fruit_pos <= 9) {
		print_spaces(3 + 3 * (inserted_fruit_pos - 5));
		print_tile(fruits[0]);
		print_spaces(3 * (4 - (inserted_fruit_pos - 5)) + 3);
	} else {
		print_spaces(21);
	}
	if (!current_player) {
		printf("->");
	} else {
		print_spaces(2);
	}
	printf("%s: %d\n\r", players[0], points[0]);
	// masodik sor
	printf("   ---------------   ");
	if (current_player) {
		printf("->");
	} else {
		print_spaces(2);
	}
	printf("%s: %d\n\r", players[1], points[1]);
	// harmadik sor
	if (inserted_fruit_pos == 4) {
		print_tile(fruits[0]);
	} else {
		print_spaces(3);
	}
	for (int i = 0; i < TABLE_SIZE; i++) {
		print_tile(table[index_by_coord(i, 0)]);
	}
	if (inserted_fruit_pos == 10) {
		print_tile(fruits[0]);
	}
	printf("\n\r");
	// negyedik sor
	printf("   ---------------   ->");
	for (int i = 0; i < 3; i++) {
		print_tile(fruits[i]);
	}
	printf("\n\r");
	// otodik sor
	if (inserted_fruit_pos == 3) {
		print_tile(fruits[0]);
	} else {
		print_spaces(3);
	}
	for (int i = 0; i < TABLE_SIZE; i++) {
		print_tile(table[index_by_coord(i, 1)]);
	}
	if (inserted_fruit_pos == 11) {
		print_tile(fruits[0]);
	} else {
		print_spaces(3);
	}
	printf("\n\r");
	// hatodik sor
	printf("   ---------------     ");
	print_tile(0);
	printf(": %d\n\r", values[0]);
	// hetedik sor
	if (inserted_fruit_pos == 2) {
		print_tile(fruits[0]);
	} else {
		print_spaces(3);
	}
	for (int i = 0; i < TABLE_SIZE; i++) {
		print_tile(table[index_by_coord(i, 2)]);
	}
	if (inserted_fruit_pos == 12) {
		print_tile(fruits[0]);
	} else {
		print_spaces(3);
	}
	printf("  ");
	print_tile(1);
	printf(": %d\n\r", values[1]);
	// nyolcadik sor
	printf("   ---------------     ");
	print_tile(2);
	printf(": %d\n\r", values[2]);
	// kilencedik sor
	if (inserted_fruit_pos == 1) {
		print_tile(fruits[0]);
	} else {
		print_spaces(3);
	}
	for (int i = 0; i < TABLE_SIZE; i++) {
		print_tile(table[index_by_coord(i, 3)]);
	}
	if (inserted_fruit_pos == 13) {
		print_tile(fruits[0]);
	} else {
		print_spaces(3);
	}
	printf("  ");
	print_tile(3);
	printf(": %d\n\r", values[3]);
	// tizedik sor
	printf("   ---------------     ");
	print_tile(4);
	printf(": %d\n\r", values[4]);
	// tizenegyedik sor
	if (inserted_fruit_pos == 0) {
		print_tile(fruits[0]);
	} else {
		print_spaces(3);
	}
	for (int i = 0; i < TABLE_SIZE; i++) {
		print_tile(table[index_by_coord(i, 4)]);
	}
	if (inserted_fruit_pos == 14) {
		print_tile(fruits[0]);
	} else {
		print_spaces(3);
	}
	printf("  ");
	print_tile(5);
	printf(": %d\n\r", values[5]);
	// tizenkettedik sor
	printf("   ---------------     ");
	print_tile(6);
	printf(": %d\n\r", values[6]);
	
	
} // print_state

// egy csemoe kiiratasa
//Levi
void print_tile(int fruit) {
	switch (fruit) {
		case 0:
			printf(SEVEN_STRING);
			break;
		case 1:
			printf(BAR_STRING);
			break;
		case 2:
			printf(CHERRY_STRING);
			break;
		case 3:
			printf(PLUM_STRING);
			break;
		case 4:
			printf(PEAR_STRING);
			break;
		case 5:
			printf(BANANA_STRING);
			break;
		case 6:
			printf(BELL_STRING);
			break;
		case 7:
			printf(EMPTY_STRING);
			break;
	}
} // print_tile

// a csempek leeseseert felelos fuggveny
//Zoli
void tile_gravity(int* table) {
	for (int i = 0; i < TABLE_SIZE; i++) {
		int j = 1;
		while (j < TABLE_SIZE) {
			if (table[index_by_coord(i, j)] == EMPTY && table[index_by_coord(i, j - 1)] != EMPTY) {
				table[index_by_coord(i, j)] = table[index_by_coord(i, j - 1)];
				table[index_by_coord(i, j - 1)] = EMPTY;
				j = 1;
			} else {
				j++;
			}
		}
	}
} // tile_gravity

// gyumolcs berakasa a tablaba
// a where valtozo mutatja, hogy melyik iranybol kerul a tablaba
//    5 6 7 8 9
// 4 |*********| 10
// 3 |         | 11
// 2 |  tabla  | 12
// 1 |         | 13
// 0 |_________| 14
//Levi 50% Zoli 50%
void insert_fruit(int* table, int where, int fruit) {
	int next = fruit;
	switch (where) {
		case 0:
		case 1:
		case 2:
		case 3:
		case 4:
			for (int i = 0; i < TABLE_SIZE; i++) {
				if (table[index_by_coord(i, 4 - where)] == EMPTY) {
					table[index_by_coord(i, 4 - where)] = next;
					break;
				} else {
					int tmp = table[index_by_coord(i, 4 - where)];
					table[index_by_coord(i, 4 - where)] = next;
					next = tmp;
				}
			}
			break;
		case 5:
		case 6:
		case 7:
		case 8:
		case 9:
			for (int i = 0; i < TABLE_SIZE; i++) {
				if (table[index_by_coord(where - 5, i)] == EMPTY) {
					table[index_by_coord(where - 5, i)] = next;
					break;
				} else {
					int tmp = table[index_by_coord(where - 5, i)];
					table[index_by_coord(where - 5, i)] = next;
					next = tmp;
				}
			}
			break;
		case 10:
		case 11:
		case 12:
		case 13:
		case 14:
			for (int i = TABLE_SIZE - 1; i >= 0; i--) {
				if (table[index_by_coord(i, where - 10)] == EMPTY) {
					table[index_by_coord(i, where - 10)] = next;
					break;
				} else {
					int tmp = table[index_by_coord(i, where - 10)];
					table[index_by_coord(i, where - 10)] = next;
					next = tmp;
				}
			}
	}
	tile_gravity(table);
} // insert_fruit

// egyezesek keresese
// egyszerre csak egyet talal meg
//Zoli
struct match find_match(int* table) {
	struct match ret;
	int length;
	int x = 0;
	int y = 0;
	int prev_char;
	// sorokban
	for (int i = 0; i < TABLE_SIZE; i++) {
		y = 0;
		x = i;
		length = 1;
		prev_char = table[index_by_coord(x, 0)];
		for (int j = 1; j < TABLE_SIZE; j++) {
			if (table[index_by_coord(i, j)] == EMPTY) continue;
			if (table[index_by_coord(i, j)] == prev_char) {
				length++;
			} else {
				if (length >= 3) {
					break;
				} else {
					length = 1;
					y = j;
					prev_char = table[index_by_coord(i, j)];
				}
			}
		}
		if (length >= 3) {
			break;
		}
	}
	
	if (length >= 3) {
		ret.direction = 0;
		ret.length = length;
		ret.x = x;
		ret.y = y;
		ret.fruit = table[index_by_coord(x, y)];
		return ret;
	}
	// oszlopokban
	for (int i = 0; i < TABLE_SIZE; i++) {
		x = 0;
		y = i;
		length = 1;
		prev_char = table[index_by_coord(0, y)];
		for (int j = 1; j < TABLE_SIZE; j++) {
			if (table[index_by_coord(j, i)] == EMPTY) continue;
			if (table[index_by_coord(j, i)] == prev_char) {
				length++;
			} else {
				if (length >= 3) {
					break;
				} else {
					length = 1;
					x = j;
					prev_char = table[index_by_coord(j, i)];
				}
			}
		}
		if (length >= 3) {
			break;
		}
	}
	if (length >= 3) {
		ret.direction = 1;
		ret.length = length;
		ret.x = x;
		ret.y = y;
		ret.fruit = table[index_by_coord(x, y)];
		return ret;
	}	
	ret.length = 0;
	return ret;
} // find_match

//Levi
int match_to_points(int* table, struct match m) {
	if (m.length == 0) {
		return 0;
	}
	for (int i = 0; i < m.length; i++) {
		table[index_by_coord(m.x + i * m.direction, m.y + i * (1 - m.direction))] = 7;
	}
	tile_gravity(table);
	return values[m.fruit] * (m.length - 2);
} // match_to_points

//Levi
void fill_fruits(int* fruits) {
	srand(time(NULL));
	for (int i = 0; i < 3; i++) {
		fruits[i] = rand() % 7;
	}
} // fill_fruits

//Zoli
int use_fruit(int* fruits) {
	srand(time(NULL));
	int used_fruit = fruits[0];
	fruits[0] = fruits[1];
	fruits[1] = fruits[2];
	fruits[2] = rand() % 7;
	return used_fruit;
} // use_fruit

//Levi
int find_winner(int* points) {
	int diff = points[0] - points[1];
	if (diff >= 250) {
		return 0;
	} else if (diff <= -250) {
		return 1;
	}
	return -1;
} // find_winner
