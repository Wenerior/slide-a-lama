#include "liblama-test.h"
#include "liblama.h"
#include <CUnit/Basic.h>
#include <CUnit/CUnit.h>

CU_SuiteInfo pnm_suites[] = {
        { "type", NULL, NULL, NULL, NULL, liblama_tests},
        CU_SUITE_INFO_NULL
};

int main() {
    if (CU_initialize_registry() != CUE_SUCCESS) {
        return -1;
    };
    if (CU_register_suites(pnm_suites) != CUE_SUCCESS) {
        return -1;
    };
    CU_basic_set_mode(CU_BRM_NORMAL);
    CU_basic_run_tests();
    CU_cleanup_registry();
    return 0;
}