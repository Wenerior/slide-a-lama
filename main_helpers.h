#ifndef _MAIN_HELPERS_H_
#define _MAIN_HELPERS_H_

#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <termios.h>

char* read_name(int length);
void clear_screen();

void term_raw_mode();
void term_reset();

#endif
