all: lama
test: utest.c
	gcc -o test utest.c liblama-test.c liblama.c -lcunit
liblama.o: liblama.c
	gcc -c liblama.c -o liblama.o

main_helpers.o: main_helpers.c
	gcc -c main_helpers.c -o main_helpers.o

main.o: main.c
	gcc -c main.c -o main.o

lama: liblama.o main.o main_helpers.o
	gcc liblama.o main.o main_helpers.o -o slide-a-lama

clean:
	rm -f liblama.o main.o main_helpers.o slide-a-lama

.PHONY: all clean
